
# password

## reeplace_all_char_with_circle.html

Objectif: renseigner un mot de passe dans un formulaire.

Dans la plus part des cas, il suffit d'utiliser le type password pour le champ de saisie du mot de passe.
Mais sur un mobile ou une tablette, le caractère saisi est affiché en clair pendant quelques instant.

Ce que je fais dans ce cas, c'est, à la saisie, remplacer chaque caractère saisi par un cercle.
J’en ai profité pour désactiver le copier/couper/coller.